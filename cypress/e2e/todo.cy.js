describe("test todo and count", () => {
    beforeEach(() => {
        cy.visit("http://localhost:3000/");
        cy.viewport(1280, 750);
    });

    it("add todo", () => {
      cy.get('button').click();
      cy.get('#title').type('test').type('{enter}');
      cy.contains('test');
    });
    it("count todos", () => {
        cy.get('button').click();
        cy.get('#title').type('test').type('{enter}');
        cy.get('#title').type('test2').type('{enter}');
        cy.contains('Total Todos: 2');
      });
    it("count todos selected", () => {
        cy.get('button').click();
        cy.get('#title').type('test').type('{enter}');
        cy.get('#title').type('test2').type('{enter}');
        cy.get('input[type="checkbox"]').check();
        cy.contains('Selected Todos: 1');
      });
  });
